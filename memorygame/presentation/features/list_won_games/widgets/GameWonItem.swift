//
// GameWonItem.swift
// memorygame
//
// Created by Jontahan Ixcayau on 25/05/23.
//

import SwiftUI

struct GameWonItem: View {
    let game: GameRecord
    
    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            detailRow(title: "Difficulty", value: game.difficulty.description)
            detailRow(title: "Attempts", value: "\(game.attemptsError)")
            detailRow(title: "Time", value: "\(String(format: "%.2f", game.secondsExpended)) seconds")
            detailRow(title: "Date", value: game.getDate())
        }
        .padding()
        .background(RoundedRectangle(cornerRadius: 10)
            .foregroundColor(.white)
            .shadow(radius: 5))
        .padding([.leading, .trailing, .bottom])
    }
    
    private func detailRow(title: String, value: String) -> some View {
        HStack {
            Text(title)
                .font(.headline)
            Spacer()
            Text(value)
                .font(.subheadline)
        }
    }
}
