//
//  wordList.swift
//  memorygame
//
//  Created by Jontahan Ixcayau on 7/04/23.
//

import Foundation

struct WordsManager {
    var WordsList = [
        "Flutter",
        "Dart",
        "Kotlin",
        "Java",
        "Jetpack Compose",
        "Swift",
        "Swift UI",
        "Objective C",
        "Node JS",
        "JS",
        "TS",
        "Angular",
        "React",
        "Python",
        "C++",
        "C#",
        "Ruby",
        "PHP",
        "HTML",
        "CSS",
        "Sass",
        "Less",
        "TypeScript",
        "CoffeeScript",
        "Vue.js",
        "Ruby on Rails",
        "Django",
        "Laravel",
        "Spring Framework",
        "Jenkins",
        "Git",
        "Docker",
        "Kubernetes",
        "Amazon Web Services (AWS)",
        "Microsoft Azure",
        "Google Cloud Platform (GCP)",
        "Firebase",
        "MongoDB",
        "MySQL",
        "PostgreSQL",
        "TensorFlow",
        "PyTorch",
        "Unity",
        "Blender",
        "Adobe Photoshop",
        "Adobe Illustrator",
        "Sketch",
        "Figma",
        "InVision",
        "Zeplin",
        "Trello",
        "Jira",
        "Asana",
        "Basecamp",
        "Slack",
        "Zoom"
    ];
    
    
    func getListShuffled(dificultyLevel: DifficultyLevel) -> [GuessWord] {
        var wordsList: [GuessWord] = []
        
        let wordsChossen = getRandomWords(dificultyLevel: dificultyLevel)
        
        
        for word in wordsChossen {
            wordsList.append(GuessWord(word: word))
            wordsList.append(GuessWord(word: word))
        }
        
        wordsList.shuffle()
        
        return wordsList
    }
    
    func getRandomWords(dificultyLevel: DifficultyLevel) -> [String] {
        let listSize = getListSizeFromDifficulty(dificultyLevel: dificultyLevel)
        var wordsChoosen: [String] = []
        
        while (wordsChoosen.count != listSize){
            let randomWord = WordsList.randomElement()
            
            if randomWord != nil && !wordsChoosen.contains(randomWord!) {
                wordsChoosen.append(randomWord!)
            }
        }
        
        return wordsChoosen
    }
    
    
    func getListSizeFromDifficulty(dificultyLevel: DifficultyLevel) -> Int {
        switch dificultyLevel {
        case .Easy:
            return 3
        case .Medium:
            return 6
        case .Hard:
            return 9
        }
    }
    
}
